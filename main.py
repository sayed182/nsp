import base64
import io

from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5 import uic
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout
import folium
from branca.element import IFrame
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import random
import cv2
import sys, time
from layout import Ui_MainWindow

class ImageReadThread(QtCore.QThread):
    signal = QtCore.pyqtSignal(object)
    def __init__(self, parent=None, videoPath="", threadIndex = 0):
        super(ImageReadThread, self).__init__(parent)
        self.videoFilePath = videoPath
        self.threadIndex = threadIndex
        self.is_running = True

    def run(self):
        print('Starting Thread..', self.threadIndex)
        cap = cv2.VideoCapture(self.videoFilePath)
        while self.is_running:
            _, im0 = cap.read()
            if (im0 is None):
                self.changePixmap.emit(None)
                break
            img_show = cv2.cvtColor(im0, cv2.COLOR_RGB2BGR)
            convertToQtFormat = QtGui.QImage(img_show.data,img_show.shape[1], img_show.shape[0], QtGui.QImage.Format_RGB888)
            p = convertToQtFormat


            self.signal.emit({"image": p, "index": self.threadIndex})
    def stop(self):
        self.is_running = False
        print(f"Thread {self.threadIndex} Stopped")
        # self.terminate()

class PotHoleDetector(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.thread = {}
        self.ui.startButton.clicked.connect(self.startThreads)
        self.ui.stopButton.clicked.connect(self.stopThreads)
        self.BASEPATH="/home/kotai001/Documents/Training Video/"
        self.videoPath = ["hiv00001.mp4", "hiv00002.mp4", "hiv00003.mp4", "hiv00004.mp4", "ch01_20210128132847.mp4"]
        self.videoFrames = [self.ui.cameraOne, self.ui.cameraTwo, self.ui.cameraThree, self.ui.cameraFour, self.ui.cameraFive]
        self.graphUpdateTimer = QtCore.QTimer()
        self.graphUpdateTimer.timeout.connect(self.plot)

    def setupLiveGraph(self):

        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.ui.horizontalLayout_2.addWidget(self.canvas)


    def plot(self):
        # random data
        data = [random.random() for i in range(20, 22)]

        # clearing old figure
        self.figure.clear()

        # create an axis
        ax = self.figure.add_subplot(111)

        # plot data
        ax.plot(data, '*-')

        # refresh canvas
        self.canvas.draw()

    def setupMap(self):
        m = folium.Map(location=[42.3601, -71.0589], zoom_start=12)
        png = '1.jpg'
        encoded = base64.b64encode(open(png, 'rb').read())
        html = '<img src="data:image/png;base64,{}">'.format
        # print(20*'-',encoded.decode('UTF-8'))
        iframe = IFrame(html(encoded.decode('UTF-8')), width=600,
                        height=600)
        popup = folium.Popup(iframe, max_width=2650)

        tooltip = 'Click For More Info'
        # Create markers
        folium.Marker([42.363600, -71.099500],
                      popup='<strong>Location One</strong>',
                      tooltip=tooltip).add_to(m),
        folium.Marker([42.333600, -71.109500],
                      popup='<strong>Location Two</strong>',
                      tooltip=tooltip,
                      icon=folium.Icon(icon='cloud')).add_to(m),
        folium.Marker([42.377120, -71.062400],
                      popup='<strong>Location Three</strong>',
                      tooltip=tooltip,
                      icon=folium.Icon(color='purple')).add_to(m),
        folium.Marker([42.374150, -71.122410],
                      popup='<strong>Location Four</strong>',
                      tooltip=tooltip,
                      icon=folium.Icon(color='green', icon='leaf')).add_to(m),
        folium.Marker([42.375140, -71.032450],
                      popup=popup,
                      tooltip=tooltip,
                      icon=folium.Icon(color='green', icon='leaf')).add_to(m),

        # save map data to data object
        data = io.BytesIO()
        m.save(data, close_file=False)

        webView = QWebEngineView()
        webView.setHtml(data.getvalue().decode())
        self.ui.horizontalLayout.addWidget(webView)






    def startThreads(self):

        for i in range(0, 5):
            self.thread[i] = ImageReadThread(parent=self, videoPath=self.BASEPATH+self.videoPath[i], threadIndex=i)
            self.thread[i].start()
            self.thread[i].signal.connect(self.runable)

        # self.setupLiveGraph()
        # self.graphUpdateTimer.start(1000 * 2)
        self.setupMap()

    def runable(self, dict):
        # for i in range(0, 5):
        if dict['image'] is not None:
            try:
                self.videoFrames[dict['index']].setPixmap(QPixmap.fromImage(dict['image']))
            except:
                self.videoFrames[dict['index']].setText("Video feed broken")
        else:
            pass
    #
    def stopThreads(self):
        for i in range(0, 5):
            try:
                self.thread[i].stop()
            except:
                pass
        self.graphUpdateTimer.stop()

app = QtWidgets.QApplication(sys.argv)
mainwindow = PotHoleDetector()
mainwindow.show()
sys.exit(app.exec_())
