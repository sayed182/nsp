import base64
import io
import os

from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5 import uic
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QFrame
import folium
from branca.element import IFrame
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import random
import cv2
import sys, time
from layout import Ui_MainWindow


class ImageReadThread(QtCore.QThread):
    signal = QtCore.pyqtSignal(object)
    def __init__(self, parent=None, videoPath="", threadIndex = 0):
        super(ImageReadThread, self).__init__(parent)
        self.videoFilePath = videoPath
        self.threadIndex = threadIndex
        self.is_running = True

    def run(self):
        print('Starting Thread..', self.threadIndex)
        cap = cv2.VideoCapture(self.videoFilePath)
        while self.is_running:
            _, im0 = cap.read()
            if (im0 is None):
                self.signal.emit({"image": None, "index": self.threadIndex})
                break
            img_show = cv2.cvtColor(im0, cv2.COLOR_RGB2BGR)
            convertToQtFormat = QtGui.QImage(img_show.data,img_show.shape[1], img_show.shape[0], QtGui.QImage.Format_RGB888)
            p = convertToQtFormat


            self.signal.emit({"image": p, "index": self.threadIndex})
    def stop(self):
        self.is_running = False
        print(f"Thread {self.threadIndex} Stopped")
        # self.terminate()

class PotHoleDetector(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.thread = {}
        self.ui.startButton.clicked.connect(self.startThreads)
        self.ui.stopButton.clicked.connect(self.stopThreads)
        self.ui.nextImage.clicked.connect(self.handleNextImage)
        self.ui.previousImage.clicked.connect(self.handlePreviousImage)

        self.BASEPATH="/home/kotai001/Desktop/PotHole Detector/"
        self.videoPath = ["hiv00001.mp4", "hiv00002.mp4", "Pothole_Detection_1.mp4", "Pothole_Detection_1.mp4", "Pothole_Detection_1.mp4"]
        self.videoFrames = [self.ui.cameraOne, self.ui.cameraTwo, self.ui.cameraThree, self.ui.cameraFive]
        self.graphUpdateTimer = QtCore.QTimer()
        self.graphUpdateTimer.timeout.connect(self.plot)
        self.fps = [0,0,0]
        self.pothole_depth = [0,0,0]
        self.galleryIndex = 0
        basepath = os.path.join(os.getcwd(), 'img')
        if os.path.isdir(basepath):
            _, _, self.filenames = next(os.walk(basepath))
        self.coordinates = [[22.5789729,88.4679328],[22.578831, 88.470691], [22.578924, 88.469968], [22.579229, 88.468566], [22.579417, 88.467386], [22.579615, 88.466302]]


        self.ui.postprocessingAction.triggered.connect(lambda: self.handleNavigation(1))
        self.ui.preprocessingAction.triggered.connect(lambda: self.handleNavigation(0))
    def handleNavigation(self, index):
        print(index)
        self.ui.stackedWidget.setCurrentIndex(index)

    def setupLiveGraph(self):

        self.figure = plt.figure()
        self.figure1 = plt.figure()
        self.figure2 = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.canvas1 = FigureCanvas(self.figure1)
        self.canvas2 = FigureCanvas(self.figure2)


        self.ui.LeftGraph.addWidget(self.canvas)
        self.ui.middleGraph.addWidget(self.canvas1)
        self.ui.rightGraph.addWidget(self.canvas2)


    def plot(self):
        # random data
        data = [random.uniform(21,22) for i in range(1, 40)]
        data1 = [random.uniform(20,22) for i in range(1, 40)]
        data2 = [random.uniform(20,21) for i in range(1, 40)]
        # data = random.uniform(20,22)
        # clearing old figure
        self.figure.clear()
        self.figure1.clear()
        self.figure2.clear()
        self.fps.append((self.fps[-1:])[0] + 1)
        # create an axis
        ax = self.figure.add_subplot(111)
        ay = self.figure.add_subplot(111)
        ax1 = self.figure1.add_subplot(111)
        ax12 = self.figure2.add_subplot(111)


        # plot data
        # ax.plot((self.fps[-1:])[0], (self.pothole_depth[-1:])[0])
        ax.plot(data, '*-')
        ax1.plot(data1, '*-')
        ax12.plot(data2, '*-')

        # refresh canvas
        self.canvas.draw()
        self.canvas1.draw()
        self.canvas2.draw()

    def setupMap(self):
        m = folium.Map(location=[22.5789729,88.4679328], zoom_start=16)
        png = '1.jpg'
        encoded = base64.b64encode(open(png, 'rb').read())
        html = '<img src="data:image/png;base64,{}">'.format
        # print(20*'-',encoded.decode('UTF-8'))
        iframe = IFrame(html(encoded.decode('UTF-8')), width=400,
                        height=400)
        popup = folium.Popup(iframe, max_width=2650)

        png1 = 'img/download.jpg'
        encoded1 = base64.b64encode(open(png, 'rb').read())
        html1 = '<img src="data:image/png;base64,{}">'.format
        # print(20*'-',encoded.decode('UTF-8'))
        iframe1 = IFrame(html1(encoded.decode('UTF-8')), width=400,
                        height=400)
        popup1 = folium.Popup(iframe1, max_width=2650)

        png2 = 'img/download (1).jpg'
        encoded2 = base64.b64encode(open(png, 'rb').read())
        html2 = '<img src="data:image/png;base64,{}">'.format
        # print(20*'-',encoded.decode('UTF-8'))
        iframe2 = IFrame(html2(encoded.decode('UTF-8')), width=400,
                         height=400)
        popup2 = folium.Popup(iframe2, max_width=2650)

        png3 = 'img/download (2).jpg'
        encoded3 = base64.b64encode(open(png, 'rb').read())
        html3 = '<img src="data:image/png;base64,{}">'.format
        # print(20*'-',encoded.decode('UTF-8'))
        iframe3 = IFrame(html3(encoded.decode('UTF-8')), width=400,
                         height=400)
        popup3 = folium.Popup(iframe3, max_width=2650)

        tooltip = 'Click For More Info'
        # Create markers
        # folium.Marker(self.coordinates[0],
        #               popup=popup3,
        #               tooltip=tooltip).add_to(m),
        # folium.Marker(self.coordinates[1],
        #               popup=popup2,
        #               tooltip=tooltip,
        #               icon=folium.Icon(icon='cloud')).add_to(m),
        # folium.Marker(self.coordinates[2],
        #               popup=popup,
        #               tooltip=tooltip,
        #               icon=folium.Icon(color='purple')).add_to(m),
        # folium.Marker(self.coordinates[3],
        #               popup=popup1,
        #               tooltip=tooltip,
        #               icon=folium.Icon(color='green', icon='leaf')).add_to(m),
        # folium.Marker(self.coordinates[4],
        #               popup=popup,
        #               tooltip=tooltip,
        #               icon=folium.Icon(color='green', icon='leaf')).add_to(m),

        folium.PolyLine([self.coordinates[0], self.coordinates[1]], color='red').add_to(m);
        folium.PolyLine([self.coordinates[1], self.coordinates[2]], color='green').add_to(m);
        folium.PolyLine([self.coordinates[2], self.coordinates[3]], color='green').add_to(m);
        folium.PolyLine([self.coordinates[3], self.coordinates[4]], color='orange').add_to(m);

        # save map data to data object
        data = io.BytesIO()
        m.save(data, close_file=False)

        webView = QWebEngineView()
        webView.setHtml(data.getvalue().decode())
        self.ui.horizontalLayout_2.addWidget(webView)






    def startThreads(self):

        for i in range(0, 4):
            self.thread[i] = ImageReadThread(parent=self, videoPath=self.BASEPATH+self.videoPath[i], threadIndex=i)
            self.thread[i].start()
            self.thread[i].signal.connect(self.runable)

        self.setupLiveGraph()
        self.graphUpdateTimer.start(1000 * 2)
        self.setupMap()

    def runable(self, dict):
        for i in range(0, 4):
            if dict['image'] is not None:
                try:
                    self.videoFrames[dict['index']].setPixmap(QPixmap.fromImage(dict['image']))
                except:
                    self.videoFrames[dict['index']].setText("Video feed broken")
            else:

                pass
    #
    def stopThreads(self):
        for i in range(0, 4):
            try:
                self.thread[i].stop()
            except:
                pass
        self.graphUpdateTimer.stop()



########################################################################################


    def handleNextImage(self):
        self.renderGalleryImage(self.filenames[self.galleryIndex])
        self.setupMapPostProcessing(coordinate=self.coordinates[self.galleryIndex])
        if self.galleryIndex < len(self.coordinates):
            self.galleryIndex += 1
        pass

    def handlePreviousImage(self):
        if self.galleryIndex >=0:
            self.galleryIndex -= 1
        self.renderGalleryImage(self.filenames[self.galleryIndex])
        self.setupMapPostProcessing(coordinate=self.coordinates[self.galleryIndex])

        pass

    def renderGalleryImage(self, imageName):
        print(imageName)
        pixmap = QPixmap(os.path.join('img/',imageName))
        self.ui.postprocessingImageLabel.setPixmap(pixmap)
        self.ui.postprocessingImageLabel.setScaledContents(True)

    def setupMapPostProcessing(self, coordinate):

        m = folium.Map(location=[22.5789729,88.4679328], zoom_start=16)
        png = '1.jpg'
        encoded = base64.b64encode(open(png, 'rb').read())
        html = '<img src="data:image/png;base64,{}">'.format
        iframe = IFrame(html(encoded.decode('UTF-8')), width=400,
                        height=400)
        popup = folium.Popup(iframe, max_width=2650)

        tooltip = 'Click For More Info'
        # Create markers
        folium.Marker(coordinate,
                      popup=popup,
                      tooltip=tooltip,
                      icon=folium.Icon(color='green', icon='leaf')).add_to(m),

        # save map data to data object
        data = io.BytesIO()
        m.save(data, close_file=False)

        webView = QWebEngineView()
        webView.setHtml(data.getvalue().decode())
        print(self.galleryIndex)
        if self.galleryIndex >= -1:
            self.ui.horizontalLayout_5.itemAt(0).widget().deleteLater()
            self.ui.horizontalLayout_5.addWidget(webView)
        else:
            self.ui.horizontalLayout_5.addWidget( webView)
        # self.ui.horizontalLayout.replaceWidget(webView)

app = QtWidgets.QApplication(sys.argv)
mainwindow = PotHoleDetector()
mainwindow.show()
sys.exit(app.exec_())
