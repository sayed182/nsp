import base64
import sys
import io
import folium  # pip install folium
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout
from PyQt5.QtWebEngineWidgets import QWebEngineView  # pip install PyQtWebEngine
from branca.element import IFrame

"""
Folium in PyQt5
"""


class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Folium in PyQt Example')
        self.window_width, self.window_height = 1600, 1200
        self.setMinimumSize(self.window_width, self.window_height)

        layout = QVBoxLayout()
        self.setLayout(layout)
        m = folium.Map(location=[42.3601, -71.0589], zoom_start=12)
        #
        # coordinate = (37.8199286, -122.4782551)
        # m = folium.Map(
        #     tiles='Stamen Terrain',
        #     zoom_start=13,
        #     location=coordinate
        # )
        png = '1.jpg'
        encoded = base64.b64encode(open(png, 'rb').read())
        html = '<img src="data:image/png;base64,{}">'.format
        # print(20*'-',encoded.decode('UTF-8'))
        iframe = IFrame(html(encoded.decode('UTF-8')), width=450,
                        height=200)
        popup = folium.Popup(iframe, max_width=2650)

        latlngs = [
            [42.363600, -71.099500],
            [42.333600, -71.109500],
            [42.377120, -71.062400]
        ];

        tooltip = 'Click For More Info'
        # Create markers
        folium.Marker([42.363600, -71.099500],
                      popup='<strong>Location One</strong>',
                      tooltip=tooltip).add_to(m),
        folium.Marker([42.333600, -71.109500],
                      popup='<strong>Location Two</strong>',
                      tooltip=tooltip,
                      icon=folium.Icon(icon='cloud')).add_to(m),
        folium.Marker([42.377120, -71.062400],
                      popup='<strong>Location Three</strong>',
                      tooltip=tooltip,
                      icon=folium.Icon(color='purple')).add_to(m),
        folium.Marker([42.374150, -71.122410],
                      popup='<strong>Location Four</strong>',
                      tooltip=tooltip,
                      icon=folium.Icon(color='green', icon='leaf')).add_to(m),
        folium.Marker([42.375140, -71.032450],
                      popup=popup,
                      tooltip=tooltip,
                      icon=folium.Icon(color='green', icon='leaf')).add_to(m),


        # save map data to data object
        data = io.BytesIO()
        m.save(data, close_file=False)

        webView = QWebEngineView()
        webView.setHtml(data.getvalue().decode())
        layout.addWidget(webView)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyleSheet('''
        QWidget {
            font-size: 35px;
        }
    ''')

    myApp = MyApp()
    myApp.show()

    try:
        sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')